# This file is a template, and might need editing before it works on your project.
FROM circleci/android:api-29-ndk AS builder

# gcc for cgo
RUN sudo apt-get update && sudo apt-get install -y --no-install-recommends \
		g++ \
		gcc \
		libc6-dev \
		make \
		pkg-config \
	&& sudo rm -rf /var/lib/apt/lists/*

ENV GOLANG_VERSION 1.14.2

RUN set -eux; \
	\
	url="https://golang.org/dl/$(curl https://golang.org/VERSION?m=text).linux-amd64.tar.gz"; \
	wget -O /tmp/go.tgz "$url"; \
	sudo tar -C /usr/local -xzf /tmp/go.tgz; \
	rm /tmp/go.tgz; \
	\
	export PATH="/usr/local/go/bin:$PATH"; \
	go version

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

